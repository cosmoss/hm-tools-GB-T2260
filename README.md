# hm-tools-GB-T2260
[![Maven Central](https://img.shields.io/maven-central/v/top.hmtools/hm-tools-gb-t2260.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22top.hmtools%22%20AND%20a:%22hm-tools-gb-t2260%22)  [![996.icu](https://img.shields.io/badge/link-996.icu-red.svg)](https://996.icu)

中华人民共和国行政划区信息工具
数据来源：
- [http://www.mca.gov.cn/article/sj/tjbz/a/](http://www.mca.gov.cn/article/sj/tjbz/a/)

参考并鸣谢：
- [https://github.com/cn/GB2260](https://github.com/cn/GB2260)
- [https://gitee.com/ansj/ansj_seg](https://gitee.com/ansj/ansj_seg)


本工具包功能：
- 根据行政代码获取行政地区信息（包括省、市、区/县）
- 根据地名查询获取行政地区信息（包括省、市、区/县）
- 可用于从地址信息中提取出省、市、区/县信息
- 获取所有省/直辖市、市、区/县层级信息集合，用于进行三级联动下拉框

# 使用说明
```
GBT2260.init();//初始化（在项目工程中，初始化一次即可）
DivisionBean bean = GBT2260.getByName("祁阳");//根据地名查询其行政地区信息，包括省、市、区/县信息
```
打印返回结果：
```
431121 湖南省 永州市 祁阳县 
```

**注意：**
- **本项目使用了 ansj 分词工具，该工具需要在使用项目的根目录下从“library”文件夹下读取字典文件，可从本项目根目录下复制，请保持文件夹结构。（本工具（hm-tools-GB-T2260）从0.0.6版本开始解决了此问题，即可以不在复制字典。）**
- **使用自定义行政区域字典时，数据须每行按“编号\t地名\r”格式，其中“编号”即唯一的特定的编码，“\t”即tab符（也可以使用英文半角逗号“,”），“地名”即地名，“\r”即换行符。**


#### 如何使用本组件的快照版本

1. 在自己的私服建立一个快照代理仓库即可。https://oss.sonatype.org/content/repositories/snapshots/
![如何使用快照版本](/images/howToUseSnapshot.jpg)

2. 将建好的参考纳入公共库
![如何使用快照版本](/images/howToUseSnapshot-2.jpg)