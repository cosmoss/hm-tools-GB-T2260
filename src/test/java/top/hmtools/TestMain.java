package top.hmtools;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import top.hmtools.beans.DivisionBean;
import top.hmtools.dictionary.Dictionary;
import top.hmtools.enums.EDataVersion;

public class TestMain {
	
	@Before
	public void before(){
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("datas/GB2260-2017.txt");
//		Dictionary.init(inputStream);
		
		Dictionary.init();
	}

	/**
	 * 通过行政区域编码获取地区信息
	 */
	@Test
	public void testgetByCode(){
		DivisionBean bean = Dictionary.getByCode("220400");
		System.out.println(bean);
		Map<String, DivisionBean> children = bean.getChildren();
		System.out.println(children);
	}
	
	/**
	 * 获取所有行政区域信息集合
	 */
	@Test
	public void testgetAll(){
		List<DivisionBean> all = Dictionary.getAll();
		for(DivisionBean ii:all){
			System.out.println(ii);
		}
		System.out.println(all.size());
		System.out.println(EDataVersion.getLast());
	}
	
	/**
	 * 根据地名获取行政区域信息
	 */
	@Test
	public void testgetByName(){
	    GBT2260.init();
	    DivisionBean bean = GBT2260.getByName("西安");
//	    System.out.println(bean);
	    List<DivisionBean> someByName = GBT2260.getSomeByName("祁阳", 1);
	    for(DivisionBean tmp:someByName){
	        System.out.println(tmp);
	    }
	}
	
	/**
	 * 测试初始化时，指定Lucene文件存放路径
	 */
	@Test
	public void testThePath(){
	    GBT2260.init("d:\\hm-gbt2260", EDataVersion.v2002);
	    DivisionBean bean = GBT2260.getByName("永州冷水滩");
	    System.out.println(bean);
	}
	
	/**
	 * 测试获取所有省份信息
	* <br>方法说明：                    testEEE
	* <br>输入参数说明：           
	* <br>
	* <br>输出参数说明：
	* <br>void           
	*
	 */
	@Test
	public void testgetAllProvince(){
	    GBT2260.init();
	    List<DivisionBean> allProvince = GBT2260.getAllProvince();
	    System.out.println(allProvince);
	}
	
	/**
	 * 测试是否省/直辖市、地级市、区/县
	 */
	@Test
	public void testIsProvinceCityDistrict(){
		GBT2260.init();
		String[] rigions = {"长沙","天津","湖南","东安县","上海","永州"};
		
		for(String rigion:rigions){
			DivisionBean divisionBean = GBT2260.getByName(rigion);
			System.out.printf("%s 是否省/直辖市？ %s ，是否地级市？ %s，是否区县？ %s", rigion,divisionBean.isProvince(),divisionBean.isCity(),divisionBean.isDistrict());
			System.out.println("==========");
		}
	}
	
	/**
	 * 测试获取GBT2260版本信息
	 */
	@Test
	public void getGB2260Version(){
		GBT2260.init();
		System.out.printf("缺省初始化版本：%s", GBT2260.getCurrentDataVersion());
		
		System.out.println("=========");
		
		GBT2260.init(EDataVersion.v2004);
		System.out.printf("手动指定初始化版本：%s", GBT2260.getCurrentDataVersion());
	}
	
	
	
	
	
	
	
	
	
}
