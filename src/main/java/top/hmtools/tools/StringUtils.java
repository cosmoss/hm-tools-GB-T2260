package top.hmtools.tools;

public class StringUtils {

	/**
	 * 是否不为空
	 * @param province
	 * @return
	 */
	public static boolean isNotEmpty(String province) {
		return province != null && province.length()>0;
	}

}
