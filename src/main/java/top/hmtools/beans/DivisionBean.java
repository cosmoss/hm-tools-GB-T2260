package top.hmtools.beans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 行政划区地名描述实体类
 * @author Jianghaibo
 *
 */
public class DivisionBean implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = -8490152017395790355L;

    /**
     * 当前地域行政代号（编码）
     */
    protected String code;
    
    /**
     * 所属省份（直辖市）名称
     */
    protected String province;
    
    /**
     * 所属省份（直辖市）行政代号（编码）
     */
    protected String provinceCode;
    
    /**
     * 所属市名称
     */
    protected String city;
    
    /**
     * 所属市行政代号（编码）
     */
    protected String cityCode;
    
    /**
     * 区（县）
     */
    protected String district;
    
    /**
     * 上级行政区代号
     */
    protected String parentCode;
    
    /**
     * 是否是省份、直辖市
     */
    protected boolean isProvince = false;
    
    /**
     * 是否是地级城市
     */
    protected boolean isCity = false;
    
    /**
     * 是否区/县
     */
    protected boolean isDistrict = false;
    
    /**
     * 下级行政区集合
     */
    protected Map<String,DivisionBean> children = new HashMap<String, DivisionBean>();

    /**
     * 当前地域行政代号（编码）
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * 当前地域行政代号（编码）
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
        try {
        	this.provinceCode=this.code.substring(0, 2)+"0000";
        	this.cityCode = this.code.substring(0, 4)+"00";
		} catch (Exception e) {
		}
        //设置是否省、市、区县标示
        if(this.code.endsWith("0000")){
        	this.isProvince=true;
        	return;
        }else if(this.code.endsWith("00")&&!this.code.endsWith("0000")){
        	this.isCity=true;
        	return;
        }else{
        	this.isDistrict=true;
        	return;
        }
    }

    /**
     * 所属省份（直辖市）名称
     * @return the province
     */
    public String getProvince() {
        return province;
    }

    /**
     * 所属省份（直辖市）名称
     * @param province the province to set
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * 所属市名称
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * 所属市名称
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * 区（县）
     * @return the district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * 区（县）
     * @param district the district to set
     */
    public void setDistrict(String district) {
        this.district = district;
    }
    
    

    /**
     * 上级行政区代号
     * @return the parentCode
     */
    public String getParentCode() {
        return parentCode;
    }

    /**
     * 上级行政区代号
     * @param parentCode the parentCode to set
     */
    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    /**
     * 下级行政区集合
     * @return the children
     */
    public Map<String, DivisionBean> getChildren() {
        return children;
    }

    /**
     * 添加一个下级行政区域信息
     * @param children the children to set
     */
    public void addChildren(DivisionBean... children) {
        for(DivisionBean bean:children){
            this.children.put(bean.getCode(), bean);
        }
    }
    
    

    public static long getSerialversionuid() {
		return serialVersionUID;
	}

    /**
     * 所属省份（直辖市）行政代号（编码）
     * @return
     */
	public String getProvinceCode() {
		return provinceCode=this.code.substring(0, 2)+"0000";
	}

	/**
	 * 所属市行政代号（编码）
	 * @return
	 */
	public String getCityCode() {
		return cityCode=this.code.substring(0, 4)+"00";
	}
	
	
	/**
	 * 获取是否是省份
	 * @return
	 */
	public boolean isProvince() {
		return isProvince;
	}

	/**
	 * 是否是地级城市
	 * @return
	 */
	public boolean isCity() {
		return isCity;
	}

	/**
	 * 是否区/县
	 * @return
	 */
	public boolean isDistrict() {
		return isDistrict;
	}

	/**
     * 方法说明
     * 输入参数说明
     * 输出参数说明
     */
    @Override
    public String toString() {
        StringBuffer result = new StringBuffer();
        if(!this.isBlank(code)){
            result.append(code);
            result.append(" ");
        }
        if(!this.isBlank(this.province)){
            result.append(this.province);
            result.append(" ");
        }
        if(!this.isBlank(city)){
            result.append(this.city);
            result.append(" ");
        }
        if(!this.isBlank(district)){
            result.append(this.district);
            result.append(" ");
        }
        return result.toString();
    }
    
    private boolean isBlank(String str){
        if(str==null || "".equals(str)){
            return true;
        }else{
            return false;
        }
    }
    
    
}
