package top.hmtools.exception;
public class InvalidCodeException extends RuntimeException {
    public InvalidCodeException(String s) {
        super(s, new Throwable());
    }
}