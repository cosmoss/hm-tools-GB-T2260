package top.hmtools.enums;

import java.util.TreeMap;

/**
 * 数据版本枚举
 * @author Pucou
 *
 */
public enum EDataVersion {

	v2002("GB2260-2002.txt"),
	v2003("GB2260-2003.txt"),
	v200306("GB2260-200306.txt"),
	v2004("GB2260-2004.txt"),
	v200403("GB2260-200403.txt"),

	v200409("GB2260-200409.txt"),
	v2005("GB2260-2005.txt"),
	v200506("GB2260-200506.txt"),
	v2006("GB2260-2006.txt"),
	v2007("GB2260-2007.txt"),

	v2008("GB2260-2008.txt"),
	v2009("GB2260-2009.txt"),
	v2010("GB2260-2010.txt"),
	v2011("GB2260-2011.txt"),
	v2012("GB2260-2012.txt"),

	v2013("GB2260-2013.txt"),
	v2014("GB2260-2014.txt"),
	v2017("GB2260-2017.txt"),
	v201807("GB2260-201807.txt")
	;
	
	private String yearVersion;
	
	EDataVersion(String yearverString){
		this.yearVersion = yearverString;
	}
	
	/**
	 * 获取最新的版本
	 * @return
	 */
	public static EDataVersion getLast(){
		TreeMap<String, EDataVersion> mm = new TreeMap<String, EDataVersion>();
		for(EDataVersion ee:EDataVersion.values()){
			mm.put(ee.yearVersion, ee);
		}
		return mm.get(mm.lastKey());
	}
	
	@Override
	public String toString() {
		return this.yearVersion;
	}
}
