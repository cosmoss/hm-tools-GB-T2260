package top.hmtools.pathToStream;

import java.io.InputStream;

import org.ansj.dic.PathToStream;

/**
 * 自定义的 从 classpath下加载
 * @author Hybomyth
 *
 */
public class ClasspathPathToStream extends PathToStream {

	@Override
	public InputStream toStream(final String path) {
		if(path==null||path.length()<1){
			throw new RuntimeException("字典路径为空");
		}
		String[] split = path.split("\\|");
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(split[1]);
		return inputStream;
	}

}
